import {firebaseConfig} from './config.js';
(
  document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
      // const model = {};
      const view = {
        init: () => {
          view.render();
        },
        render: () => {

        },
      };
      const controller = {
        init: () => {

        },
      };
      firebase.initializeApp(firebaseConfig);
      const ui = new firebaseui.auth.AuthUI(firebase.auth());
      ui.start('#firebaseui-auth-container', {
        signInOptions: [
          firebase.auth.GoogleAuthProvider.PROVIDER_ID,
          firebase.auth.GithubAuthProvider.PROVIDER_ID,
          {
            provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
            defaultCountry: 'IN',
          },
        ],
        signInSuccessUrl: 'admin.html',
      });
      firebase.auth().onAuthStateChanged((admin) => {
        if (admin) {
          // User is signed in.
          controller.init();
          // ...
        } else {
          // User is signed out.
          // ...
        }
      });
    }
  }
)();
