(
  document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
      // const model = {};
      const view = {
        manager: document.getElementById('manager'),
        admin: document.getElementById('admin'),
        employee: document.getElementById('employee'),
        init: () => {
          view.manager.onclick = () => {
            controller.managerLoginPage();
          };
          view.admin.onclick = () => {
            controller.adminLoginPage();
          };
          view.employee.onclick = () => {
            controller.employeeLoginPage();
          };
          view.render();
        },
        render: () => {
        },
      };
      const controller = {
        managerLoginPage: () => {
          window.location.href = './manager-login.html';
        },
        adminLoginPage: () => {
          window.location.href = './admin-login.html';
        },
        employeeLoginPage: () => {
          window.location.href = './employee-login.html';
        },
        init: () => {
          view.init();
        },
      };
      controller.init();
    }
  }
)();
